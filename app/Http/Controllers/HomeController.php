<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $skills = [
            ['name' => 'HTML5 & CSS3', 'rating' => rand(55, 100)],
            ['name' => 'WEB DESIGN', 'rating' => rand(40, 100)],
            ['name' => 'JAVASCRIPT', 'rating' => rand(30, 100)],
            ['name' => 'PHP', 'rating' => rand(55, 100)],
        ];

        return view('profile')->with('skills', $skills)->render();
    }
}
