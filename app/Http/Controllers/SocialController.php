<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator,Redirect,Response,File;
use Socialite;
use App\User;

class SocialController extends Controller
{
    /**
     * Redirect to provider Oauth [Facebook, Google, Instagram]
     *
     * @param [type] $provider
     * @return void
     */
    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Callback from provider
     *
     * @param [type] $provider
     * @return void
     */
    public function callback($provider)
    {
      $getInfo = Socialite::driver($provider)->user();
      $user = $this->createUser($getInfo,$provider);
      auth()->login($user);
      return redirect()->to('/home');
    }

    /**
     * Create the User
     *
     * @param [type] $getInfo
     * @param [type] $provider
     * @return void
     */
    protected function createUser($getInfo,$provider)
    {
        $user = User::where('provider_id', $getInfo->id)->first();
        if (!$user) {
            $user = User::create([
                'name'     => $getInfo->name,
                'email'    => isset($getInfo->email) ? $getInfo->email : $getInfo->id.'@plannthat.com',
                'provider' => $provider,
                'provider_id' => $getInfo->id
            ]);
        }
        return $user;
    }
}
