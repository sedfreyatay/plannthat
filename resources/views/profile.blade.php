@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <section class="intro-section">
                <div class="container">

                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-md-1 col-lg-2"></div>
                        <div class="col-md-10 col-lg-8">
                            <div class="intro">
                                <div class="profile-img">
                                    <img src="https://graph.facebook.com/{{auth()->user()->provider_id}}/picture?type=large" alt="">
                                </div>
                                <h2><b>{{auth()->user()->name}}</b></h2>
                                <h4 class="font-email">{{auth()->user()->email}}</h4>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        @foreach ($skills as $skill)
                            <div class="col-sm-6 col-md-6 col-lg-3">
                                <div class="radial-prog-area margin-b-30">
                                    <div class="radial-progress">
                                        <h6 class="progress-title">{{$skill['name']}}</h6>

                                        <svg viewBox="0 0 100 100">
                                            <path d="M 50,50 m 0,-48.5 a 48.5,48.5 0 1 1 0,97 a 48.5,48.5 0 1 1 0,-97" stroke="#eee" stroke-width="1" fill-opacity="0"></path>
                                            <path d="M 50,50 m 0,-48.5 a 48.5,48.5 0 1 1 0,97 a 48.5,48.5 0 1 1 0,-97" stroke="rgb(253,173,1)" stroke-width="2.999784" fill-opacity="0" style="stroke-dasharray: 304.844, 304.844; stroke-dashoffset: {{ (100 - $skill['rating']) * 3 }};"></path>
                                        </svg>

                                        <div class="progressbar-text">{{$skill['rating']}}</div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
@endsection
